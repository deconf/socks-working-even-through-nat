#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#ifndef DEBUG
//#pragma comment(linker,"/ALIGN:512 ")
//#pragma comment(linker,"/merge:.data=.text /merge:.rdata=.text")
//#pragma comment(linker,"/entry:m")
//#pragma comment(linker,"/section:.text,WRX")
#pragma optimize("gsy",on)
#endif

#include <windows.h>
#include <Winsock2.h>
#include <stdio.h>
#include <stdlib.h>

#include "configuration.h"
#include "memory.h"
#include "low_sockets.h"
#include "debug.h"
#include "bscs_client.h"