void fdeb(char *msg,...);
char* fmterr(void);
void hexdump(char *buffer, int size);

#ifdef FRELEASE
#define deb //
#else
#define deb fdeb
#endif